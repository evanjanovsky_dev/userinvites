import { useState, useEffect } from 'react';
import styled from 'styled-components';
import ChevRight from './chev-right.svg';
import Standard from './standard.svg';
import Administrators from './administrators.svg';

interface TeamMember {
  id: number;
  status: Status;
  user: UserShortData;
  role: Role;
}

type Role = 'Administrator' | 'Standard';

type Status = 'request' | 'pending' | 'approved' | 'declined' | 'invited';

interface UserShortData {
  id: number;
  name: string;
  lastName: string;
  phone: string;
  email: string;
}

interface Invite {
  id: number;
  phone: string;
  role: Role;
}
interface MergedUsersInvite {
  id: number;
  status: Status;
  user: Partial<UserShortData>;
  role: Role;
}

const UserAndInviteList = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [users, setUsers] = useState<TeamMember[]>([]);
  const [invites, setInvites] = useState<Invite[]>([]);
  const [administrators, setAdministrators] = useState<MergedUsersInvite[]>([]);
  const [standardUsers, setStandardUsers] = useState<MergedUsersInvite[]>([]);

  const fetchUsers = () => {
    fetch('https://6189f82c34b4f400177c4350.mockapi.io/users')
      .then((response) => response.json())
      .then((data) => {
        setIsLoading(false);
        setUsers(data);
      })
      .catch((error) => {
        setIsLoading(false);
        //TODO: Display Error Toast message
        console.log(error);
      });
  };

  const fetchInvites = () => {
    fetch('https://6189f82c34b4f400177c4350.mockapi.io/invites')
      .then((response) => response.json())
      .then((data) => {
        setIsLoading(false);
        setInvites(data);
      })
      .catch((error) => {
        setIsLoading(false);
        //TODO: Display Error Toast message
        console.log(error);
      });
  };

  // On page load fetch the data
  useEffect(() => {
    fetchUsers();
    fetchInvites();
  }, []);

  useEffect(() => {
    // Add an 'invite' status to invites and format to be similar to users object
    const formattedInvites = invites.map((invite) => {
      return {
        id: invite.id,
        status: 'invited' as Status,
        user: {
          phone: invite.phone,
        },
        role: invite.role,
      };
    });

    // Merge the two arrays
    const mergedArrays = [...formattedInvites, ...users];

    // Create new arrays by role
    const admins = mergedArrays.filter((user) => user.role === 'Administrator');
    const standards = mergedArrays.filter((user) => user.role === 'Standard');

    // set the state
    setStandardUsers(standards);
    setAdministrators(admins);
  }, [users, invites]);

  const renderListItem = (item: MergedUsersInvite) => {
    return (
      <ListItem
        showStatusTag={item?.status === 'invited'}
        key={`${item.id}_${item.user.phone}`}
      >
        {item?.status === 'invited'
          ? `${item?.user?.phone}`
          : `${item?.user?.name} ${item?.user?.lastName}`}
        {item?.status === 'invited' && (
          <StyledStatusTag>{item.status}</StyledStatusTag>
        )}
        <img src={ChevRight} alt="right-arrow" />
      </ListItem>
    );
  };

  return (
    <PageContainer>
      {isLoading ? (
        <span>Loading...</span>
      ) : (
        <>
          <StyledSectionLabel>
            <img src={Administrators} alt="shield" />
            <span>Administrators</span>
          </StyledSectionLabel>
          <ListContainer>
            {administrators.length ? (
              administrators.map((item) => renderListItem(item))
            ) : (
              <ListItem>No Administrator Users or Invites</ListItem>
            )}
          </ListContainer>
          <StyledSectionLabel>
            <img src={Standard} alt="user" />
            <span>Standard Users</span>
          </StyledSectionLabel>
          <ListContainer>
            {standardUsers.length ? (
              standardUsers.map((item) => renderListItem(item))
            ) : (
              <ListItem>No Standard Users or Invites</ListItem>
            )}
          </ListContainer>
        </>
      )}
    </PageContainer>
  );
};

export default UserAndInviteList;

const PageContainer = styled.div`
  background-color: #fafafd;
  padding: 1rem;
  min-height: 100vh;
  max-width: 375px;
  max-height: 100vh;
  overflow-y: scroll;
  margin: auto;
  font-family: 'Inter', sans-serif;
`;

const ListContainer = styled.div`
  background-color: #fff;
  border-radius: 1rem;
  padding: 0 1rem;
  margin-bottom: 2rem;
  transition: all 0.4s ease;
`;

const ListItem = styled.div<{ showStatusTag: boolean }>`
  padding: 2rem 0;
  display: grid;
  grid-template-columns: ${(props) =>
    props.showStatusTag ? '1fr auto auto' : '1fr auto'};
  align-items: center;
  grid-gap: 0.5rem;
  border-bottom: 1px solid #f8f8f9;
  font-style: normal;
  font-weight: 600;
  font-size: 11px;
  line-height: 24px;
  letter-spacing: -0.02em;
  color: ${(props) => (props.showStatusTag ? '#828B97' : '#3e4959')};
`;

const StyledSectionLabel = styled.div`
  margin: 1rem 0;
  img {
    margin-right: 0.5rem;
  }
  span {
    color: #828b97;
    font-style: normal;
    font-weight: 600;
    font-size: 11px;
    line-height: 24px;
    letter-spacing: -0.02em;
  }
`;

const StyledStatusTag = styled.span`
  text-transform: capitalize;
  background-color: #eeeff2;
  color: #3e4959;
  padding: 2px 10px;
  border-radius: 27px;
  margin-right: 1rem; ;
`;
